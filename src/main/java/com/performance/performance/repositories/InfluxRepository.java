package com.performance.performance.repositories;

import com.performance.performance.InfluxDBTemplate;
import com.performance.performance.controllers.InfluxDBController;

import java.util.List;

import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Kushtrim.Bytyqi on 11/3/2017.
 */
@Repository
public class InfluxRepository
{
	private static Logger logger = LoggerFactory.getLogger(InfluxDBController.class);

	@Autowired
	private InfluxDBTemplate<Point> influxDBTemplate;

	public List<QueryResult.Result> getMetrics(String version, int servers, int rnumber,
			String envName)
	{
		String criteria = ";" + rnumber + ";" + version + ";" + servers + ";" + envName;
		final Query q = new Query(
				"SELECT request, percentiles95 FROM \"gatling\" WHERE \"request\" "
						+ "<> 'allRequests' AND \"request\" =~ /." + criteria
						+ "./ AND \"status\" = 'ok' GROUP BY \"request\" ",
				influxDBTemplate.getDatabase());

		QueryResult results = influxDBTemplate.getConnection().query(q);
		return results.getResults();
	}
}
