package com.performance.performance.repositories;

import com.performance.performance.entities.Releases;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Kushtrim.Bytyqi on 11/3/2017.
 */
public interface ReleasesRepository extends CrudRepository<Releases, Integer>
{
	@Query(value = "select r.version from releases r where id = (select max(id) from releases)", nativeQuery = true)
	String getLastReleaseName();

	@Query(value = "select * from releases r where id = (select max(id) from releases)", nativeQuery = true)
	Releases getLastRelease();
}
