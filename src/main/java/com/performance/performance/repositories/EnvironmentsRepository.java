package com.performance.performance.repositories;

import com.performance.performance.entities.Environment;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by Kushtrim.Bytyqi on 11/3/2017.
 */
public interface EnvironmentsRepository extends CrudRepository<Environment, Integer>
{
}
