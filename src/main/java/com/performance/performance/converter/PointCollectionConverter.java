package com.performance.performance.converter;

import java.util.List;

import org.influxdb.dto.Point;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by Kushtrim.Bytyqi on 11/3/2017.
 */

public interface PointCollectionConverter<T> extends Converter<T, List<Point>>
{

}