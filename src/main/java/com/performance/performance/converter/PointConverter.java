package com.performance.performance.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.influxdb.dto.Point;

/**
 * Created by Kushtrim.Bytyqi on 11/3/2017.
 */

public class PointConverter implements PointCollectionConverter<Point>
{
	@Override
	public List<Point> convert(final Point source)
	{
		final ArrayList<Point> list = new ArrayList<>(1);
		Collections.addAll(list, source);
		return list;
	}
}
