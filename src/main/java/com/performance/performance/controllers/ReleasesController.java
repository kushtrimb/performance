package com.performance.performance.controllers;

import com.performance.performance.entities.ReleasesDao;
import com.performance.performance.services.ReleasesService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Kushtrim.Bytyqi on 11/3/2017.
 */
@RestController
public class ReleasesController
{
	@Autowired
	private ReleasesService releasesService;

	@CrossOrigin
	@RequestMapping(value = "/releases", method = RequestMethod.GET)
	public List<ReleasesDao> releases()
	{
		return releasesService.listAllReleases();
	}

	@RequestMapping(value = "/environments", method = RequestMethod.GET)
	public String environments()
	{
		return releasesService.listAllEnvironments().toString();
	}
}
