package com.performance.performance.controllers;

import com.performance.performance.entities.ReleaseComparisonDAO;
import com.performance.performance.services.InfluxService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Kushtrim.Bytyqi on 11/3/2017.
 */

@RestController
@RequestMapping("/metrics")
public class InfluxDBController
{

	@Autowired
	private InfluxService influxService;

	@CrossOrigin
	@RequestMapping(value = "diff/{first}/{second}/{servers}", method = RequestMethod.GET)
	public ReleaseComparisonDAO diff(@PathVariable Integer first, @PathVariable Integer second,
			@PathVariable Integer servers)
	{
		return influxService.diff(first, second, servers);
	}
}
