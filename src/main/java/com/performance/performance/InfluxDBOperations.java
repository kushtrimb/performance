package com.performance.performance;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.influxdb.dto.Pong;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;

/**
 * Created by Kushtrim.Bytyqi on 11/3/2017.
 */
public interface InfluxDBOperations<T>
{
	/**
	 * Executes a query against the database.
	 *
	 * @param query
	 *            the query to execute
	 * @return a List of time series data matching the query
	 */
	QueryResult query(final Query query);

	/**
	 * Executes a query against the database.
	 *
	 * @param query
	 *            the query to execute
	 * @param timeUnit
	 *            the time unit to be used for the query
	 * @return a List of time series data matching the query
	 */
	QueryResult query(final Query query, final TimeUnit timeUnit);

	/**
	 * Execute a streaming query against the database.
	 *
	 * @param query
	 *            the query to execute
	 * @param chunkSize
	 *            the number of QueryResults to process in one chunk
	 * @param consumer
	 *            the consumer to invoke for each received QueryResult
	 */
	void query(final Query query, final int chunkSize, final Consumer<QueryResult> consumer);

	/**
	 * Ping the database.
	 *
	 * @return the response of the ping execution
	 */
	Pong ping();

	/**
	 * Return the version of the connected database.
	 *
	 * @return the version String, otherwise unknown
	 */
	String version();
}
