package com.performance.performance;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.influxdb.dto.Pong;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.springframework.data.influxdb.converter.PointCollectionConverter;
import org.springframework.util.Assert;

public class InfluxDBTemplate<T> extends InfluxDBAccessor implements InfluxDBOperations<T>
{
	private PointCollectionConverter<T> converter;

	public InfluxDBTemplate()
	{

	}

	public InfluxDBTemplate(final InfluxDBConnectionFactory connectionFactory,
			final PointCollectionConverter<T> converter)
	{
		setConnectionFactory(connectionFactory);
		setConverter(converter);
	}

	public void setConverter(final PointCollectionConverter<T> converter)
	{
		this.converter = converter;
	}

	@Override
	public void afterPropertiesSet()
	{
		super.afterPropertiesSet();
		Assert.notNull(converter, "PointCollectionConverter is required");
	}

	@Override
	public QueryResult query(final Query query)
	{
		return getConnection().query(query);
	}

	@Override
	public QueryResult query(final Query query, final TimeUnit timeUnit)
	{
		return getConnection().query(query, timeUnit);
	}

	@Override
	public void query(Query query, int chunkSize, Consumer<QueryResult> consumer)
	{
		getConnection().query(query, chunkSize, consumer);
	}

	@Override
	public Pong ping()
	{
		return getConnection().ping();
	}

	@Override
	public String version()
	{
		return getConnection().version();
	}
}