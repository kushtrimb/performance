package com.performance.performance.entities;

import java.util.List;

/**
 * Created by Kushtrim.Bytyqi on 11/6/2017.
 */
public class ReleaseDAO
{
	private String version;
	private List<ScenarioObj> releaseMetrics;

	public String getVersion()
	{
		return version;
	}

	public void setVersion(String version)
	{
		this.version = version;
	}

	public List<ScenarioObj> getReleaseMetrics()
	{
		return releaseMetrics;
	}

	public void setReleaseMetrics(List<ScenarioObj> releaseMetrics)
	{
		this.releaseMetrics = releaseMetrics;
	}

	public String toString()
	{
		return version + releaseMetrics;
	}
}
