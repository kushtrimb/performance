package com.performance.performance.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * Created by Kushtrim.Bytyqi on 11/3/2017.
 */
@Entity
public class Releases
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String version;

	private int runnumber;

	private int servers;

	@OneToOne
	@JoinColumn(name = "env")
	private Environment env;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getVersion()
	{
		return version;
	}

	public void setVersion(String version)
	{
		this.version = version;
	}

	public int getRunnumber()
	{
		return runnumber;
	}

	public void setRunnumber(int runnumber)
	{
		this.runnumber = runnumber;
	}

	public int getServers()
	{
		return servers;
	}

	public void setServers(int servers)
	{
		this.servers = servers;
	}

	public String toString()
	{
		return "version: " + version +"; "+ "runNr: "+runnumber;
	}

	public Environment getEnv()
	{
		return env;
	}

	public void setEnv(Environment env)
	{
		this.env = env;
	}
}
