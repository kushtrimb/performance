package com.performance.performance.entities;

import java.util.List;

/**
 * Created by Kushtrim.Bytyqi on 11/6/2017.
 */
public class ScenarioObj
{
	private String request;
	private List<String> metric;
	private String metricAvg;

	public String getRequest()
	{
		return request;
	}

	public void setRequest(String request)
	{
		this.request = request;
	}

	public List<String> getMetric()
	{
		return metric;
	}

	public void setMetric(List<String> metric)
	{
		this.metric = metric;
	}

	public String toString()
	{
		return "request:"+ request  + "metric:" + metric;
	}

	public String getMetricAvg()
	{
		return metricAvg;
	}

	public void setMetricAvg(String metricAvg)
	{
		this.metricAvg = metricAvg;
	}
}
