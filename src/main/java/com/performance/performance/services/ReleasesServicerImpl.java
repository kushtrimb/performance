package com.performance.performance.services;

import com.performance.performance.entities.Environment;
import com.performance.performance.entities.Releases;
import com.performance.performance.entities.ReleasesDao;
import com.performance.performance.repositories.EnvironmentsRepository;
import com.performance.performance.repositories.ReleasesRepository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Kushtrim.Bytyqi on 11/3/2017.
 */
@Service
public class ReleasesServicerImpl implements ReleasesService
{
	@Autowired
	private ReleasesRepository releasesRepository;

	@Autowired
	private EnvironmentsRepository environmentsRepository;

	public List<ReleasesDao> listAllReleases()
	{
		Iterable<Releases> itr = releasesRepository.findAll();
		List<ReleasesDao> rd = new ArrayList<>();
		for (Releases rl : itr)
		{
			ReleasesDao rdao = new ReleasesDao();
			rdao.setId(rl.getId());
			rdao.setVersion(rl.getVersion());
			rd.add(rdao);
		}
		return rd;
	}

	public Iterable<Environment> listAllEnvironments()
	{
		return environmentsRepository.findAll();
	}
}
