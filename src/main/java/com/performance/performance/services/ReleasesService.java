package com.performance.performance.services;

import com.performance.performance.entities.Environment;
import com.performance.performance.entities.ReleasesDao;

import java.util.List;

/**
 * Created by Kushtrim.Bytyqi on 11/3/2017.
 */
public interface ReleasesService
{
	List<ReleasesDao> listAllReleases();

	Iterable<Environment> listAllEnvironments();
}
