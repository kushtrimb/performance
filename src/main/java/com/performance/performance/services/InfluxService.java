package com.performance.performance.services;

/**
 * Created by Kushtrim.Bytyqi on 11/3/2017.
 */
import com.performance.performance.entities.ReleaseComparisonDAO;
import com.performance.performance.entities.ReleasesCompareDAO;

public interface InfluxService
{
	String showMetrics(String version, int servers, int rnnumber, String envName);

	ReleasesCompareDAO comparison(String firstReleas, String secondReleas, int servers,
			int firstRn, int secondRn, String firstenvName, String secondenvName);

	ReleaseComparisonDAO diff(int first, int second, int servers);
}
