package com.performance.performance.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.performance.performance.entities.ComparisonDAO;
import com.performance.performance.entities.MetricCompDao;
import com.performance.performance.entities.ReleaseComparisonDAO;
import com.performance.performance.entities.ReleaseDAO;
import com.performance.performance.entities.Releases;
import com.performance.performance.entities.ReleasesCompareDAO;
import com.performance.performance.entities.ScenarioObj;
import com.performance.performance.repositories.InfluxRepository;
import com.performance.performance.repositories.ReleasesRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.influxdb.dto.QueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Kushtrim.Bytyqi on 11/3/2017.
 */
@Service
public class InfluxServiceImpl implements InfluxService
{
	@Autowired
	private InfluxRepository influxRepository;

	@Autowired
	private ReleasesRepository releasesRepository;

	private List<ScenarioObj> scenarios;

	private Map<String, List<Double>> map;

	private Map<String, List<String>> mapUNr;

	public String showMetrics(String version, int servers, int rnnumber, String envName)
	{
		scenarios = new ArrayList<>();
		map = new HashMap<>();
		List<QueryResult.Result> metrics = influxRepository.getMetrics(version, servers, rnnumber, envName);

		for (QueryResult.Result res : metrics)
		{
			for (QueryResult.Series series : res.getSeries())
			{
				for (Object value : series.getValues())
				{
					String[] str = value.toString().split(",");

					String reqName = formatReq(str[1]); //.split(";")[0];

					if (map.get(reqName) == null)
					{
						List<Double> mtr = new ArrayList<>();
						mtr.add(formatMetric(str[2]));
						map.put(reqName, mtr);
					}
					else
					{
						map.get(reqName).add(formatMetric(str[2]));
					}
				}
			}
		}

		return processMetrics(releasesRepository.getLastReleaseName());
	}

	private double formatMetric(String metric)
	{
		return Double.valueOf(metric.replace("]", "").replace(" ", "")) / 1000;
	}

	private String formatReq(String req)
	{
		return req.replace("[", "").replace(" ", "");
	}

	private String processMetrics(String lastRelease)
	{
		ObjectMapper mapper = new ObjectMapper();
		ReleaseDAO rd = new ReleaseDAO();
		rd.setVersion(lastRelease);

		System.out.println(map);

		mapUNr = new HashMap<>();

		for (Map.Entry<String, List<Double>> entry : map.entrySet())
		{
			String reqName = entry.getKey().split(";")[0];
			List<Double> metrics = entry.getValue();

			List<String> metricsAvg = new ArrayList<>();

			double mtrAvg = 0;
			int count = metrics.size();

			for (Double mtrcs : metrics)
			{
				mtrAvg += mtrcs;
			}

			mtrAvg = mtrAvg / count;
			metricsAvg.add(String.format("%.3f", mtrAvg));

			if (mapUNr.get(reqName) == null)
			{
				mapUNr.put(reqName, metricsAvg);
			}
			else
			{
				mapUNr.get(reqName).add(String.format("%.3f", mtrAvg));
			}
		}

		for (Map.Entry<String, List<String>> entry : mapUNr.entrySet())
		{
			double metricsAvg = 0;
			ScenarioObj scObj = new ScenarioObj();
			scObj.setRequest(entry.getKey());
			scObj.setMetric(entry.getValue());
			for (String val : entry.getValue())
			{
				metricsAvg += Double.valueOf(val);
			}

			metricsAvg = metricsAvg / entry.getValue().size();
			scObj.setMetricAvg(String.format("%.3f", metricsAvg));
			scenarios.add(scObj);
		}

		rd.setReleaseMetrics(scenarios);
		String releasesMetrics = "";

		try
		{
			releasesMetrics = mapper.writeValueAsString(rd);
		}
		catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}

		return releasesMetrics;
	}

	public ReleasesCompareDAO comparison(String firstReleas, String secondReleas, int servers,
			int firstRn, int secondRn, String firstenvName, String secondEnvName)
	{
		ObjectMapper mapper = new ObjectMapper();
		ReleaseDAO rd = new ReleaseDAO();
		ReleaseDAO rdSec = new ReleaseDAO();
		ReleasesCompareDAO rc = new ReleasesCompareDAO();
		String firstmMetrics = showMetrics(firstReleas, servers, firstRn, firstenvName);
		String secondMetrics = showMetrics(secondReleas, servers, secondRn, secondEnvName);

		try
		{
			rd = mapper.readValue(firstmMetrics, ReleaseDAO.class);
			rdSec = mapper.readValue(secondMetrics, ReleaseDAO.class);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		rc.setFirstRelease(rd);
		rc.setSecondRelease(rdSec);
		return rc;
	}

	public ReleaseComparisonDAO diff(int first, int second, int servers)
	{

		Releases firstReleas = releasesRepository.findOne(first);
		Releases secondReleas = releasesRepository.findOne(second);

		ReleasesCompareDAO rcdao = comparison(firstReleas.getVersion(), secondReleas.getVersion(),
			servers, firstReleas.getRunnumber(), secondReleas.getRunnumber(),firstReleas.getEnv().getName(),
				secondReleas.getEnv().getName());

		Map<String, ComparisonDAO> cdmap = new HashMap();
		ReleaseComparisonDAO rcd = new ReleaseComparisonDAO();

		List<ScenarioObj> firstRelease = rcdao.getFirstRelease().getReleaseMetrics();
		List<ScenarioObj> secondRelease = rcdao.getSecondRelease().getReleaseMetrics();

		for (int i = 0; i < firstRelease.size(); i++)
		{
			rcd = new ReleaseComparisonDAO();
			rcd.setFirstVersion(firstReleas.getVersion());
			rcd.setSecondVersion(secondReleas.getVersion());

			for (int j = 0; j < firstRelease.get(i).getMetric().size(); j++)
			{
				ComparisonDAO cda = new ComparisonDAO();
				MetricCompDao mcd = new MetricCompDao();
				cda.setEndpoint(firstRelease.get(i).getRequest());
				cda.setAvgDiff(firstRelease.get(i).getMetricAvg());
				cda.setMetricPerUserNR(new ArrayList<>());
				mcd.setFirst(firstRelease.get(i).getMetric().get(j));
				cda.getMetricPerUserNR().add(mcd);
				if (cdmap.get(firstRelease.get(i).getRequest()) == null)
				{
					cdmap.put(firstRelease.get(i).getRequest(), cda);
				}
				else
				{
					cdmap.get(firstRelease.get(i).getRequest()).getMetricPerUserNR().add(mcd);
				}
			}

			for (int j = 0; j < secondRelease.get(i).getMetric().size(); j++)
			{
				ComparisonDAO cda = new ComparisonDAO();
				MetricCompDao mcd = new MetricCompDao();
				cda.setMetricPerUserNR(new ArrayList<>());
				cda.setEndpoint(secondRelease.get(i).getRequest());
				cda.setAvgDiff(String.valueOf(Double.valueOf(secondRelease.get(i).getMetricAvg())
						- Double.valueOf(secondRelease.get(i).getMetricAvg())));
				mcd.setSecond(secondRelease.get(i).getMetric().get(j));
				cda.getMetricPerUserNR().add(mcd);

				if (cdmap.get(secondRelease.get(i).getRequest()) == null)
				{
					cdmap.put(secondRelease.get(i).getRequest(), cda);
				}
				else
				{
					cdmap.get(firstRelease.get(i).getRequest()).getMetricPerUserNR().get(j)
						.setSecond(secondRelease.get(i).getMetric().get(j));
					cdmap.get(firstRelease.get(i).getRequest()).setAvgDiff(cda.getAvgDiff());
				}
			}
		}

		List<ComparisonDAO> cdlIst = new ArrayList<>();

		for (Map.Entry<String, ComparisonDAO> entry : cdmap.entrySet())
		{
			cdlIst.add(entry.getValue());
		}

		rcd.setComparisons(cdlIst);

		return rcd;
	}
}